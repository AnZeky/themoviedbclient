﻿using Microsoft.Practices.Unity;
using NLog;
using Prism.Modularity;
using Prism.Regions;
using TheMovieDb.Domain.Helpers;
using TheMovieDb.Photo.Views;

namespace TheMovieDb.Photo
{
    public class PhotoModule : IModule
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;

        public PhotoModule(IUnityContainer container, IRegionManager regionManager)
        {
            _container = container;
            _regionManager = regionManager;
        }

        public void Initialize()
        {
            logger.Debug("PhotoModule - Initializing");
            _container.RegisterType<object, PosterView>(nameof(PosterView));

            _regionManager.RegisterViewWithRegion(RegionsType.PosterMovieRegion, typeof (PosterView));
            _regionManager.RegisterViewWithRegion(RegionsType.PosterPersonRegion, typeof (PosterView));
        }
    }
}
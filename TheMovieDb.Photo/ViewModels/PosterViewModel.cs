﻿using System.Collections.ObjectModel;
using Prism.Mvvm;
using Prism.Regions;
using TheMovieDb.Domain.Helpers;
using TheMovieDb.Domain.Models;
using TheMovieDb.Domain.Services.Interfaces;

namespace TheMovieDb.Photo.ViewModels
{
    public class PosterViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
    {
        private readonly IDataService _service;

        public PosterViewModel(IDataService service)
        {
            _service = service;
        }

        public ObservableCollection<Image> Images { get; set; }

        async void INavigationAware.OnNavigatedTo(NavigationContext navigationContext)
        {
            using (var busyScope = new BusyScope())
            {
                var id = int.Parse(navigationContext.Parameters["ID"].ToString());
                var type = (MediaType) navigationContext.Parameters["MEDIATYPE"];

                Images =
                    new ObservableCollection<Image>(await _service.Image.GetPosterImagesAsync(id, type, busyScope.Token));
                OnPropertyChanged("Images");
            }
        }

        bool INavigationAware.IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        void INavigationAware.OnNavigatedFrom(NavigationContext navigationContext)
        {
        }

        public bool KeepAlive => false;
    }
}
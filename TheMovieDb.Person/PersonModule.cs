﻿using Microsoft.Practices.Unity;
using NLog;
using Prism.Modularity;
using Prism.Regions;
using TheMovieDb.Domain.Helpers;
using TheMovieDb.Person.Views;

namespace TheMovieDb.Person
{
    public class PersonModule : IModule
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;

        public PersonModule(IUnityContainer container, IRegionManager regionManager)
        {
            _container = container;
            _regionManager = regionManager;
        }

        public void Initialize()
        {
            logger.Debug("PersonModule - Initializing");
            _container.RegisterType<object, PersonsCreditView>(nameof(PersonsCreditView));
            _container.RegisterType<object, PersonView>(nameof(PersonView));
            _container.RegisterType<object, PersonsView>(nameof(PersonsView));

            _regionManager.RegisterViewWithRegion(RegionsType.MainRegion, typeof (PersonsCreditView));
            _regionManager.RegisterViewWithRegion(RegionsType.SearchPersonsTabRegion, typeof (PersonsView));
            _regionManager.RegisterViewWithRegion(RegionsType.MovieCastRegion, typeof (PersonsCreditView));
            _regionManager.RegisterViewWithRegion(RegionsType.MainRegion, typeof (PersonView));
        }
    }
}
﻿using System;
using Prism.Mvvm;
using Prism.Regions;
using TheMovieDb.Domain.Helpers;
using TheMovieDb.Domain.Models;
using TheMovieDb.Domain.Services.Interfaces;

namespace TheMovieDb.Person.ViewModels
{
    public class PersonViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
    {
        private readonly IRegionManager _regionManager;
        private readonly IDataService _service;

        public PersonViewModel(IDataService service, IRegionManager manager)
        {
            _regionManager = manager;
            _service = service;
        }

        public Domain.Models.Person Person { get; set; }

        async void INavigationAware.OnNavigatedTo(NavigationContext navigationContext)
        {
            using (var busyScope = new BusyScope())
            {
                var id = int.Parse(navigationContext.Parameters["ID"].ToString());
                Person = await _service.Person.GetPersonAsync(id, busyScope.Token);
                OnPropertyChanged("Person");

                var query = new NavigationParameters
                {
                    {"ID", id},
                    {"MEDIATYPE", MediaType.Persons},
                    {"CREDITTYPE", CreditType.Cast}
                };
                _regionManager.RequestNavigate(
                    RegionsType.PersonCastMovieRegion,
                    new Uri("ListMovieView", UriKind.Relative), query);

                query = new NavigationParameters
                {
                    {"ID", id},
                    {"MEDIATYPE", MediaType.Persons},
                    {"CREDITTYPE", CreditType.Crew}
                };
                _regionManager.RequestNavigate(
                    RegionsType.PersonCrewMovieRegion,
                    new Uri("ListMovieView", UriKind.Relative), query);

                _regionManager.RequestNavigate(
                    RegionsType.PosterPersonRegion,
                    new Uri("PosterView", UriKind.Relative), query);
            }
        }

        bool INavigationAware.IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        void INavigationAware.OnNavigatedFrom(NavigationContext navigationContext)
        {
        }

        public bool KeepAlive
        {
            get { return false; }
        }
    }
}
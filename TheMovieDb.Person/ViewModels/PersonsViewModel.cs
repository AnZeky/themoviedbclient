﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Prism.Mvvm;
using Prism.Regions;
using TheMovieDb.Domain.Models;

namespace TheMovieDb.Person.ViewModels
{
    public class PersonsViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
    {
        public static MediaType Type { get; set; } = MediaType.Persons;

        public ObservableCollection<Domain.Models.Person> Persons { get; set; }

        void INavigationAware.OnNavigatedTo(NavigationContext navigationContext)
        {
            Persons = new ObservableCollection<Domain.Models.Person>((IEnumerable<Domain.Models.Person>) navigationContext.Parameters["PERSONS"]);
            OnPropertyChanged("Persons");
        }

        bool INavigationAware.IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        void INavigationAware.OnNavigatedFrom(NavigationContext navigationContext)
        {
        }

        public bool KeepAlive => false;
    }
}
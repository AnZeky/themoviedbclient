﻿using System.Collections.ObjectModel;
using Prism.Mvvm;
using Prism.Regions;
using TheMovieDb.Domain.Helpers;
using TheMovieDb.Domain.Models;
using TheMovieDb.Domain.Models.Interfaces;
using TheMovieDb.Domain.Services.Interfaces;

namespace TheMovieDb.Person.ViewModels
{
    public class PersonsCreditViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
    {
        private readonly IDataService _service;

        public PersonsCreditViewModel(IDataService service)
        {
            _service = service;
        }

        public ObservableCollection<IImage> Credits { get; set; }

        async void INavigationAware.OnNavigatedTo(NavigationContext navigationContext)
        {
            using (var busyScope = new BusyScope())
            {
                var id = int.Parse(navigationContext.Parameters["ID"].ToString());
                var mediaType = (MediaType) navigationContext.Parameters["MEDIATYPE"];
                var creditType = (CreditType) navigationContext.Parameters["CREDITTYPE"];
                switch (mediaType)
                {
                    case MediaType.Movies:
                        Credits =
                            new ObservableCollection<IImage>(
                                await _service.Movie.GetCreditsAcync(id, creditType, busyScope.Token));
                        break;
                    case MediaType.Persons:
                        Credits =
                            new ObservableCollection<IImage>(
                                await _service.Person.GetCreditsAcync(id, creditType, busyScope.Token));
                        break;
                    default:
                        return;
                }

                OnPropertyChanged("Credits");
            }
        }

        bool INavigationAware.IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        void INavigationAware.OnNavigatedFrom(NavigationContext navigationContext)
        {
        }

        public bool KeepAlive
        {
            get { return false; }
        }
    }
}
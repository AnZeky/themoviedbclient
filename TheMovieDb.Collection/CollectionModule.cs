﻿using Microsoft.Practices.Unity;
using NLog;
using Prism.Modularity;
using Prism.Regions;
using TheMovieDb.Collection.Views;
using TheMovieDb.Domain.Helpers;

namespace TheMovieDb.Collection
{
    public class CollectionModule : IModule
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;

        public CollectionModule(RegionManager regionManager, IUnityContainer container)
        {
            _regionManager = regionManager;
            _container = container;
        }

        public void Initialize()
        {
            logger.Debug("CollectionModule - Initializing");
            _container.RegisterType<object, CollectionsView>(nameof(CollectionsView));
            _container.RegisterType<object, CollectionView>(nameof(CollectionView));
            _container.RegisterType<object, SearchCollectionsView>(nameof(SearchCollectionsView));

            _regionManager.RegisterViewWithRegion(RegionsType.MainRegion, typeof (CollectionsView));
            _regionManager.RegisterViewWithRegion(RegionsType.MainRegion, typeof (CollectionView));
            _regionManager.RegisterViewWithRegion(RegionsType.SearchCollectionsTabRegion, typeof (SearchCollectionsView));
        }
    }
}
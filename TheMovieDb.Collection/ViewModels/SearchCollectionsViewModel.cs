﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Prism.Mvvm;
using Prism.Regions;

namespace TheMovieDb.Collection.ViewModels
{
    public class SearchCollectionsViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
    {
        public ObservableCollection<Domain.Models.Collection> Collections { get; set; }

        void INavigationAware.OnNavigatedTo(NavigationContext navigationContext)
        {
            var collections = (IEnumerable<Domain.Models.Collection>) navigationContext.Parameters["COLLECTIONS"];
            Collections =
                new ObservableCollection<Domain.Models.Collection>(collections);
            OnPropertyChanged("Collections");
        }

        bool INavigationAware.IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        void INavigationAware.OnNavigatedFrom(NavigationContext navigationContext)
        {
        }

        public bool KeepAlive => false;
    }
}
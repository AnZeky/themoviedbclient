﻿using System;
using Prism.Mvvm;
using Prism.Regions;
using TheMovieDb.Domain.Helpers;
using TheMovieDb.Domain.Services.Interfaces;

namespace TheMovieDb.Collection.ViewModels
{
    public class CollectionViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
    {
        private readonly IRegionManager _regionManager;
        private readonly IDataService _service;

        public CollectionViewModel(IRegionManager regionManager, IDataService service)
        {
            _regionManager = regionManager;
            _service = service;
        }

        public Domain.Models.Collection Collection { get; set; }

        async void INavigationAware.OnNavigatedTo(NavigationContext navigationContext)
        {
            using (var busyScope = new BusyScope())
            {
                var id = int.Parse(navigationContext.Parameters["ID"].ToString());
                Collection = await _service.Movie.GetCollectionAsync(id, busyScope.Token);

                var query = new NavigationParameters
                {
                    {"MOVIES", Collection.Parts}
                };
                _regionManager.RequestNavigate(
                    RegionsType.CollectionMovieRegion,
                    new Uri("MoviesView", UriKind.Relative), query);

                OnPropertyChanged("Collection");
            }
        }

        bool INavigationAware.IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        void INavigationAware.OnNavigatedFrom(NavigationContext navigationContext)
        {
        }

        public bool KeepAlive => false;
    }
}
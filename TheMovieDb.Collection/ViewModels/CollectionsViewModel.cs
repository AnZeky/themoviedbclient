﻿using System;
using NLog;
using Prism.Mvvm;
using Prism.Regions;
using TheMovieDb.Domain.Helpers;
using TheMovieDb.Domain.Services.Interfaces;

namespace TheMovieDb.Collection.ViewModels
{
    public class CollectionsViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IRegionManager _regionManager;
        private readonly IDataService _service;

        public CollectionsViewModel(IDataService service, IRegionManager regionManager)
        {
            _service = service;
            _regionManager = regionManager;
        }

        async void INavigationAware.OnNavigatedTo(NavigationContext navigationContext)
        {
            try
            {
                using (var busyScope = new BusyScope())
                {
                    var query = new NavigationParameters
                    {
                        {"MOVIES", await _service.Movie.GetTopRatedAsync(1, busyScope.Token)}
                    };
                    _regionManager.RequestNavigate(
                        RegionsType.TopRatedMoviesRegion,
                        new Uri("MoviesView", UriKind.Relative), query);

                    busyScope.Token.ThrowIfCancellationRequested();
                    query = new NavigationParameters
                    {
                        {"MOVIES", await _service.Movie.GetNowPlayingAsync(1, busyScope.Token)}
                    };
                    _regionManager.RequestNavigate(
                        RegionsType.NowPlayingMoviesRegion,
                        new Uri("MoviesView", UriKind.Relative), query);

                    busyScope.Token.ThrowIfCancellationRequested();
                    query = new NavigationParameters
                    {
                        {"MOVIES", await _service.Movie.GetPopularAsync(1, busyScope.Token)}
                    };
                    _regionManager.RequestNavigate(
                        RegionsType.PopularMoviesRegion,
                        new Uri("MoviesView", UriKind.Relative), query);

                    busyScope.Token.ThrowIfCancellationRequested();
                    query = new NavigationParameters
                    {
                        {"MOVIES", await _service.Movie.GetUpcomingAsync(1, busyScope.Token)}
                    };
                    _regionManager.RequestNavigate(
                        RegionsType.UpcomingMoviesRegion,
                        new Uri("MoviesView", UriKind.Relative), query);
                }
            }
            catch (OperationCanceledException ex)
            {
                logger.Error(ex.Message);
            }
        }

        bool INavigationAware.IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        void INavigationAware.OnNavigatedFrom(NavigationContext navigationContext)
        {
        }
        
        public bool KeepAlive => false;
    }
}
﻿using Microsoft.Practices.Unity;
using NLog;
using Prism.Modularity;
using Prism.Regions;
using TheMovieDb.Domain.Helpers;
using TheMovieDb.Movie.Views;

namespace TheMovieDb.Movie
{
    public class MovieModule : IModule
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;

        public MovieModule(IUnityContainer container, IRegionManager regionManager)
        {
            _container = container;
            _regionManager = regionManager;
        }

        public void Initialize()
        {
            logger.Debug("MovieModule - Initializing");
            _container.RegisterType<object, MoviesView>(nameof(MoviesView));
            _container.RegisterType<object, MovieView>(nameof(MovieView));
            _container.RegisterType<object, ListMovieView>(nameof(ListMovieView));
            _container.RegisterType<object, GenresView>(nameof(GenresView));

            _regionManager.RegisterViewWithRegion(RegionsType.MainRegion, typeof (MoviesView));
            _regionManager.RegisterViewWithRegion(RegionsType.SearchMovieTabRegion, typeof (MoviesView));
            _regionManager.RegisterViewWithRegion(RegionsType.MainRegion, typeof (MovieView));
            _regionManager.RegisterViewWithRegion(RegionsType.PersonCastMovieRegion, typeof (ListMovieView));
        }
    }
}
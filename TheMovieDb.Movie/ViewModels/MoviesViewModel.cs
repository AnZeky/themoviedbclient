using System.Collections.Generic;
using System.Collections.ObjectModel;
using Prism.Mvvm;
using Prism.Regions;
using TheMovieDb.Domain.Models;

namespace TheMovieDb.Movie.ViewModels
{
    public class MoviesViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
    {
        public ObservableCollection<Domain.Models.Movie> Movies { get; set; }

        void INavigationAware.OnNavigatedTo(NavigationContext navigationContext)
        {
            Movies = new ObservableCollection<Domain.Models.Movie>((IEnumerable<Domain.Models.Movie>) navigationContext.Parameters["MOVIES"]);
            OnPropertyChanged("Movies");
        }

        bool INavigationAware.IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        void INavigationAware.OnNavigatedFrom(NavigationContext navigationContext)
        {
        }

        public bool KeepAlive => false;
    }
}
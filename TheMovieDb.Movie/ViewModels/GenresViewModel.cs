﻿using System;
using Prism.Mvvm;
using Prism.Regions;
using TheMovieDb.Domain.Helpers;
using TheMovieDb.Domain.Models;
using TheMovieDb.Domain.Services.Interfaces;

namespace TheMovieDb.Movie.ViewModels
{
    public class GenresViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
    {
        private readonly IRegionManager _regionManager;
        private readonly IDataService _service;

        public GenresViewModel(IRegionManager regionManager, IDataService service)
        {
            _regionManager = regionManager;
            _service = service;
        }

        public Movies Movies { get; set; }
        public Genre Genre { get; set; }

        async void INavigationAware.OnNavigatedTo(NavigationContext navigationContext)
        {
            using (var busyScope = new BusyScope())
            {
                Genre = (Genre) navigationContext.Parameters["GENRE"];
                OnPropertyChanged("Genre");
                Movies = await _service.Movie.GetGenresAsync(Genre.Id, busyScope.Token);
                OnPropertyChanged("Movies");

                var query = new NavigationParameters
                {
                    {"MOVIES", Movies.Results}
                };
                _regionManager.RequestNavigate(
                    RegionsType.GenresMovieRegion,
                    new Uri("MoviesView", UriKind.Relative), query);
            }
        }

        bool INavigationAware.IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        void INavigationAware.OnNavigatedFrom(NavigationContext navigationContext)
        {
        }

        public bool KeepAlive => false;
    }
}
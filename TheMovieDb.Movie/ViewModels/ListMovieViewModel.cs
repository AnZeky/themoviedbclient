﻿using System.Collections.ObjectModel;
using System.Linq;
using Prism.Mvvm;
using Prism.Regions;
using TheMovieDb.Domain.Helpers;
using TheMovieDb.Domain.Models;
using TheMovieDb.Domain.Models.Interfaces;
using TheMovieDb.Domain.Services.Interfaces;

namespace TheMovieDb.Movie.ViewModels
{
    public class ListMovieViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
    {
        private readonly IDataService _service;

        public ListMovieViewModel(IDataService service)
        {
            _service = service;
        }

        public ObservableCollection<IImage> Credits { get; set; }
        public CreditType CreditType { get; set; }

        async void INavigationAware.OnNavigatedTo(NavigationContext navigationContext)
        {
            using (var busyScope = new BusyScope())
            {
                var id = int.Parse(navigationContext.Parameters["ID"].ToString());
                var mediaType = (MediaType) navigationContext.Parameters["MEDIATYPE"];
                CreditType = (CreditType) navigationContext.Parameters["CREDITTYPE"];

                switch (mediaType)
                {
                    case MediaType.Movies:
                        Credits =
                            new ObservableCollection<IImage>(
                                await _service.Movie.GetCreditsAcync(id, CreditType, busyScope.Token, false));
                        break;
                    case MediaType.Persons:
                        var personCredit =
                            await _service.Person.GetCreditsAcync(id, CreditType, busyScope.Token, false);
                        Credits =
                            new ObservableCollection<IImage>(personCredit.OrderByDescending(x => x.ReleaseDate));
                        break;
                    default:
                        return;
                }

                OnPropertyChanged("Credits");
                OnPropertyChanged("CreditType");
            }
        }

        bool INavigationAware.IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        void INavigationAware.OnNavigatedFrom(NavigationContext navigationContext)
        {
        }

        public bool KeepAlive => false;
    }
}
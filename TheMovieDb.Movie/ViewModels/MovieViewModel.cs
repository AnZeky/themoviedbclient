﻿using System;
using Prism.Mvvm;
using Prism.Regions;
using TheMovieDb.Domain.Helpers;
using TheMovieDb.Domain.Models;
using TheMovieDb.Domain.Services.Interfaces;

namespace TheMovieDb.Movie.ViewModels
{
    public class MovieViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
    {
        private readonly IRegionManager _regionManager;
        private readonly IDataService _service;

        public MovieViewModel(IDataService service, IRegionManager manager)
        {
            _regionManager = manager;
            _service = service;
        }

        public Domain.Models.Movie Movie { get; set; }

        async void INavigationAware.OnNavigatedTo(NavigationContext navigationContext)
        {
            using (var busyScope = new BusyScope())
            {
                var id = int.Parse(navigationContext.Parameters["ID"].ToString());
                Movie = await _service.Movie.GetMovieAsync(id, busyScope.Token);
                OnPropertyChanged("Movie");

                var query = new NavigationParameters
                {
                    {"ID", id},
                    {"MEDIATYPE", MediaType.Movies},
                    {"CREDITTYPE", CreditType.Cast}
                };
                _regionManager.RequestNavigate(
                    RegionsType.MovieCastRegion,
                    new Uri("PersonsCreditView", UriKind.Relative), query);

                query = new NavigationParameters
                {
                    {"ID", id},
                    {"MEDIATYPE", MediaType.Movies},
                    {"CREDITTYPE", CreditType.Crew}
                };
                _regionManager.RequestNavigate(
                    RegionsType.MovieCrewRegion,
                    new Uri("PersonsCreditView", UriKind.Relative), query);

                _regionManager.RequestNavigate(
                    RegionsType.PosterMovieRegion,
                    new Uri("PosterView", UriKind.Relative), query);
            }
        }

        bool INavigationAware.IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        void INavigationAware.OnNavigatedFrom(NavigationContext navigationContext)
        {
        }

        public bool KeepAlive => false;
    }
}
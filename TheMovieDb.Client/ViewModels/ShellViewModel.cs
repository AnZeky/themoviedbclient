﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using MahApps.Metro.Controls.Dialogs;
using NLog;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using TheMovieDb.Domain.Events;
using TheMovieDb.Domain.Helpers;
using TheMovieDb.Domain.Models;
using TheMovieDb.Domain.Services.Interfaces;

namespace TheMovieDb.Client.ViewModels
{
    public class ShellViewModel : BindableBase
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IDialogCoordinator _dialogCoordinator;
        private readonly IRegion _mainRegion;
        private readonly IRegionManager _regionManager;
        private readonly IDataService _service;
        private Account _account;
        private ProgressDialogController _progressDialogController;

        public ShellViewModel(IDataService service, IEventAggregator eventAggregator, IRegionManager regionManager,
            IDialogCoordinator dialogCoordinator)
        {
            _regionManager = regionManager;
            _dialogCoordinator = dialogCoordinator;
            _service = service;

            var busyRingEvent = eventAggregator.GetEvent<BusyRingEvent>();
            busyRingEvent.Subscribe(ActionBusyRing, ThreadOption.UIThread);

            _mainRegion = regionManager.Regions[RegionsType.MainRegion];
            _mainRegion.NavigationService.Navigated += NavigationServiceOnNavigatedMainRedion;
            BackCommand = new DelegateCommand(Back, CanBack);
            AuthenticationCommand = new DelegateCommand(Authenticate);
            SearchCommand = new DelegateCommand(Search);
            CollectionsCommand = new DelegateCommand(Collections);
        }

        public Account Account
        {
            get { return _account; }
            set
            {
                _account = value;
                OnPropertyChanged();
            }
        }

        public ICommand BackCommand { get; set; }
        public ICommand SearchCommand { get; set; }
        public ICommand AuthenticationCommand { get; set; }
        public ICommand CollectionsCommand { get; set; }
        public string Title => "TheMovieDb Client";
        public bool NotAuthenticated { get; private set; } = true;

        private async void Authenticate()
        {
            logger.Debug("Authenticate command");
            var result = await _dialogCoordinator.ShowLoginAsync(this, "Authentication", "Enter your credentials");
            if (result != null)
            {
                var account = await _service.Service.Login(result.Username, result.Password);
                if (account != null)
                {
                    NotAuthenticated = false;
                    OnPropertyChanged("NotAuthenticated");
                    Account = account;
                }
            }
        }

        private void Collections()
        {
            logger.Debug("Collections command");
            _regionManager.RequestNavigate(RegionsType.MainRegion,
                new Uri("CollectionsView", UriKind.Relative));
        }

        private void NavigationServiceOnNavigatedMainRedion(object sender,
            RegionNavigationEventArgs regionNavigationEventArgs)
        {
            (BackCommand as DelegateCommand)?.RaiseCanExecuteChanged();
        }

        private bool CanBack()
        {
            logger.Debug("Collections command");
            return _mainRegion.NavigationService.Journal.CanGoBack;
        }

        private void Back()
        {
            logger.Debug("Navigation Back command");
            _mainRegion.NavigationService.Journal.GoBack();
        }

        private async void ActionBusyRing(BusyRing busyRing)
        {
            if (busyRing.IsWait)
            {
                BusyScope.New = false;
                logger.Debug($"Show progress dialog");
                _progressDialogController =
                    await _dialogCoordinator.ShowProgressAsync(this, "Please wait...", "Loading content", true);
                _progressDialogController.SetIndeterminate();
                _progressDialogController.Canceled += ProgressDialogCanceled;
            }
            else
            {
                await Application.Current.Dispatcher.InvokeAsync(
                    async () =>
                    {
                        if (_progressDialogController.IsOpen)
                        {
                            logger.Debug($"Hide progress dialog");
                            await _progressDialogController.CloseAsync();
                        }
                        else
                        {
                            await Task.Delay(1000);
                            logger.Debug($"Close not opened progress dialog");
                            ProgressDialogCanceled(null, null);
                        }
                    },
                    DispatcherPriority.Background);
            }
        }

        private async void ProgressDialogCanceled(object sender, EventArgs eventArgs)
        {
            logger.Debug($"Canselled progress dialog");
            BusyScope.Cancel();
            if (_progressDialogController.IsOpen)
                await _progressDialogController.CloseAsync();
        }

        private void Search()
        {
            logger.Debug("Search command");
            _regionManager.RequestNavigate(RegionsType.MainRegion,
                new Uri("SearchView", UriKind.Relative));
        }
    }
}
﻿using System.Configuration;
using TheMovieDb.Domain.Helpers.Interfaces;

namespace TheMovieDb.Client.Helpers
{
    public class Settings : ISettings
    {
        public string Lang
        {
            get { return ConfigurationManager.AppSettings["Lang"]; }
            set { ConfigurationManager.AppSettings["Lang"] = value; }
        }

        public string ApiKey
        {
            get { return ConfigurationManager.AppSettings["ApiKey"]; }
            set { ConfigurationManager.AppSettings["ApiKey"] = value; }
        }
    }
}
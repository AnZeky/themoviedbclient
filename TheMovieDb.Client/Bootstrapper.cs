﻿using System.Windows;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Practices.Unity;
using NLog;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Unity;
using TheMovieDb.Client.Helpers;
using TheMovieDb.Client.Modules;
using TheMovieDb.Client.ViewModels;
using TheMovieDb.Client.Views;
using TheMovieDb.Collection;
using TheMovieDb.Domain.Helpers;
using TheMovieDb.Domain.Helpers.Interfaces;
using TheMovieDb.Domain.Services;
using TheMovieDb.Domain.Services.Interfaces;
using TheMovieDb.Movie;
using TheMovieDb.Person;
using TheMovieDb.Photo;
using TheMovieDb.Search;

namespace TheMovieDb.Client
{
    internal class Bootstrapper : UnityBootstrapper
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        protected override DependencyObject CreateShell()
        {
            return Container.Resolve<Shell>();
        }

        protected override void InitializeShell()
        {
            Application.Current.MainWindow = (Window) Shell;
            Application.Current.MainWindow.DataContext = Container.Resolve<ShellViewModel>();
            Application.Current.MainWindow.Show();
        }

        protected override void InitializeModules()
        {
            logger.Debug("Initializing Modules");
            GlobalCommands.Initialization(Container.Resolve<IRegionManager>());
            BusyScope.Initialization(Container.Resolve<IEventAggregator>());
            Container.Resolve<MovieModule>().Initialize();
            Container.Resolve<PersonModule>().Initialize();
            Container.Resolve<PhotoModule>().Initialize();
            Container.Resolve<SearchModule>().Initialize();
            Container.Resolve<CollectionModule>().Initialize();
            Container.Resolve<MainModule>().Initialize();
        }

        protected override void ConfigureContainer()
        {
            ViewModelLocationProvider.SetDefaultViewModelFactory(type => Container.Resolve(type));
            RegisterTypeIfMissing(typeof (ISettings), typeof (Settings), true);
            RegisterTypeIfMissing(typeof (IDialogCoordinator), typeof (DialogCoordinator), false);
            RegisterTypeIfMissing(typeof (IDataService), typeof (DataService), false);
            base.ConfigureContainer();
        }
    }
}
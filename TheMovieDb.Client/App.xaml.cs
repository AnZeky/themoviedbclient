﻿using System;
using System.Windows;
using NLog;

namespace TheMovieDb.Client
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            Logger log = LogManager.GetCurrentClassLogger();

            log.Info("Version: {0}", Environment.Version);
            log.Info("OS: {0}", Environment.OSVersion);
            log.Info("Command: {0}", Environment.CommandLine);
            
            var bootstrapper = new Bootstrapper();
            bootstrapper.Run();
        }
    }
}
﻿using System;
using Microsoft.Practices.Unity;
using NLog;
using Prism.Modularity;
using Prism.Regions;
using TheMovieDb.Client.Views;
using TheMovieDb.Domain.Helpers;

namespace TheMovieDb.Client.Modules
{
    public class MainModule : IModule
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;

        public MainModule(IUnityContainer container, IRegionManager regionManager)
        {
            _container = container;
            _regionManager = regionManager;
        }

        public void Initialize()
        {
            logger.Debug("MainModule - Initializing");
            _container.RegisterType<object, Shell>(nameof(Shell));

            _regionManager.RequestNavigate(RegionsType.MainRegion,
                new Uri("CollectionsView", UriKind.Relative));
        }
    }
}
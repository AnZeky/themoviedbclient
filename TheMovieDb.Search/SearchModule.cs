﻿using Microsoft.Practices.Unity;
using Prism.Modularity;
using Prism.Regions;
using TheMovieDb.Domain.Helpers;
using TheMovieDb.Search.Views;

namespace TheMovieDb.Search
{
    public class SearchModule : IModule
    {
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;

        public SearchModule(IUnityContainer container, IRegionManager regionManager)
        {
            _container = container;
            _regionManager = regionManager;
        }

        public void Initialize()
        {
            _container.RegisterType<object, SearchView>(nameof(SearchView));

            _regionManager.RegisterViewWithRegion(RegionsType.MainRegion, typeof (SearchView));
        }
    }
}
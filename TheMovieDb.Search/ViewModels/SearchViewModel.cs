﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using NLog;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using TheMovieDb.Domain.Helpers;
using TheMovieDb.Domain.Models;
using TheMovieDb.Domain.Models.Interfaces;
using TheMovieDb.Domain.Services.Interfaces;

namespace TheMovieDb.Search.ViewModels
{
    public class SearchViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IRegionManager _regionManager;
        private readonly IDataService _service;
        private int _activeTab;
        private IEnumerable<Collection> _collections;
        private bool _emptyResult;
        private IEnumerable<Movie> _movies;
        private IPagedResult _pagedResult;
        private IEnumerable<Domain.Models.Person> _persons;
        private string _resultQuery;
        private string _searchQuery;

        public SearchViewModel(IDataService service, IRegionManager manager)
        {
            _regionManager = manager;
            _service = service;
            SearchCommand = new DelegateCommand(ExecuteSearch);
            CurrenType = MediaType.Movies;
        }

        public ICommand SearchCommand { get; }

        public string SearchQuery
        {
            get { return _searchQuery; }
            set
            {
                _searchQuery = value;
                OnPropertyChanged();
            }
        }

        public string ResultQuery
        {
            get { return _resultQuery; }
            set
            {
                _resultQuery = value;
                OnPropertyChanged();
            }
        }

        public bool EmptyResult
        {
            get { return _emptyResult; }
            set
            {
                _emptyResult = value;
                OnPropertyChanged();
            }
        }

        public MediaType CurrenType { get; set; }

        public int ActiveTab
        {
            get { return _activeTab; }
            set
            {
                _activeTab = value;
                OnPropertyChanged();
            }
        }

        public IPagedResult PagedResult
        {
            get { return _pagedResult; }
            set
            {
                _pagedResult = value;
                OnPropertyChanged();
            }
        }

        public IEnumerable<Movie> Movies
        {
            get { return _movies; }
            set
            {
                _movies = value;
                OnPropertyChanged();
            }
        }

        public IEnumerable<Domain.Models.Person> Persons
        {
            get { return _persons; }
            set
            {
                _persons = value;
                OnPropertyChanged();
            }
        }

        public IEnumerable<Collection> Collections
        {
            get { return _collections; }
            set
            {
                _collections = value;
                OnPropertyChanged();
            }
        }

        void INavigationAware.OnNavigatedTo(NavigationContext navigationContext)
        {
        }

        bool INavigationAware.IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        void INavigationAware.OnNavigatedFrom(NavigationContext navigationContext)
        {
        }

        public bool KeepAlive => false;

        private async void ExecuteSearch()
        {
            if (string.IsNullOrEmpty(SearchQuery))
                return;
            using (var busyScope = new BusyScope())
            {
                logger.Debug($"Search {SearchQuery} in {CurrenType}");
                Movies = null;
                Persons = null;
                Collections = null;
                PagedResult = null;
                ResultQuery = string.Empty;
                EmptyResult = false;
                NavigationParameters query;
                switch (CurrenType)
                {
                    case MediaType.Movies:
                        var pagedMovies = await _service.Movie.SearchAsync(SearchQuery, busyScope.Token);
                        if (pagedMovies.TotalCount == 0)
                        {
                            EmptyResult = true;
                            break;
                        }

                        PagedResult = pagedMovies;
                        Movies = pagedMovies.Results;
                        ResultQuery = SearchQuery;
                        ActiveTab = (int) MediaType.Movies;

                        query = new NavigationParameters
                        {
                            {"MOVIES", Movies}
                        };
                        _regionManager.RequestNavigate(
                            RegionsType.SearchMovieTabRegion,
                            new Uri("MoviesView", UriKind.Relative), query);
                        break;
                    case MediaType.Persons:
                        var pagedPerson = await _service.Person.SearchAsync(SearchQuery, busyScope.Token);
                        if (pagedPerson.TotalCount == 0)
                        {
                            EmptyResult = true;
                            break;
                        }

                        PagedResult = pagedPerson;
                        Persons = pagedPerson.Results;
                        ResultQuery = SearchQuery;
                        ActiveTab = (int) MediaType.Persons;

                        query = new NavigationParameters
                        {
                            {"PERSONS", Persons}
                        };
                        _regionManager.RequestNavigate(
                            RegionsType.SearchPersonsTabRegion,
                            new Uri("PersonsView", UriKind.Relative), query);
                        break;
                    case MediaType.Collections:

                        var collections = await _service.Movie.SearchCollectionAsync(SearchQuery, busyScope.Token);
                        if (collections.TotalCount == 0)
                        {
                            EmptyResult = true;
                            break;
                        }
                        PagedResult = collections;
                        Collections = collections.Results;
                        ResultQuery = SearchQuery;
                        ActiveTab = (int) MediaType.Collections;

                        query = new NavigationParameters
                        {
                            {"COLLECTIONS", collections.Results}
                        };
                        _regionManager.RequestNavigate(
                            RegionsType.SearchCollectionsTabRegion,
                            new Uri("SearchCollectionsView", UriKind.Relative), query);
                        break;
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.TMDb;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using NLog;
using TheMovieDb.Domain.Helpers.Interfaces;
using TheMovieDb.Domain.Models;
using TheMovieDb.Domain.Models.Interfaces;
using TheMovieDb.Domain.Repositories.Interfaces;
using Image = TheMovieDb.Domain.Models.Image;
using Movie = TheMovieDb.Domain.Models.Movie;

namespace TheMovieDb.Domain.Repositories
{
    public class ImageRepository : IRepository
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public ImageRepository(ISettings settings)
        {
            Settings = settings;
        }

        public ISettings Settings { get; set; }

        public async Task<IEnumerable<Image>> GetPosterImagesAsync(int id, MediaType type, CancellationToken token)
        {
            logger.Debug($"Getting {type} poster images {id}");
            using (var client = new ServiceClient(Settings.ApiKey))
            {
                IEnumerable<System.Net.TMDb.Image> tmdbImages;
                switch (type)
                {
                    case MediaType.Movies:
                        tmdbImages = (await client.Movies.GetImagesAsync(id, Settings.Lang, token)).Posters;
                        break;
                    case MediaType.Persons:
                        tmdbImages = await client.People.GetImagesAsync(id, token);
                        break;
                    default:
                        tmdbImages = null;
                        break;
                }

                var images = Mapper.Map<IList<Image>>(tmdbImages);

                if (images.Count == 0)
                {
                    images.Add(
                        new Image
                        {
                            FilePath = @"/TheMovieDb.Client;component/Assets/anonymous.jpg"
                        });
                }
                else
                    await DownloadImagesAsync(images, type, token);
                return images;
            }
        }

        public async Task DownloadImagesAsync(IEnumerable<IImage> images, MediaType path, CancellationToken token)
        {
            try
            {
                foreach (var img in images)
                {
                    token.ThrowIfCancellationRequested();
                    try
                    {
                        if (img.ImagePath == null)
                        {
                            img.ImagePath = @"/TheMovieDb.Client;component/Assets/anonymous.jpg";
                            continue;
                        }
                        var filepath = Path.Combine("Cache", path.ToString(), img.ImagePath.TrimStart('/'));
                        if (!File.Exists(filepath))
                        {
                            await DownloadImageAsync(img.ImagePath, filepath, token);
                        }
                        img.ImagePath = filepath;
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceError(ex.Message);
                        img.ImagePath = @"/TheMovieDb.Client;component/Assets/anonymous.jpg";
                    }
                }
            }
            catch (OperationCanceledException ex)
            {
                logger.Error(ex.Message);
            }
        }

        public async Task<string> DownloadImageAsync(string images, MediaType path, CancellationToken token)
        {
            try
            {
                if (images == null)
                {
                    return @"/TheMovieDb.Client;component/Assets/anonymous.jpg";
                }
                var filepath = Path.Combine("Cache", path.ToString(), images.TrimStart('/'));
                if (!File.Exists(filepath))
                {
                    await DownloadImageAsync(images, filepath, token);
                }
                return filepath;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                return @"/TheMovieDb.Client;component/Assets/anonymous.jpg";
            }
        }

        public async Task DownloadImageAsync(IImage imageObject, CancellationToken token)
        {
            try
            {
                if (imageObject.ImagePath == null)
                {
                    imageObject.ImagePath = @"/TheMovieDb.Client;component/Assets/anonymous.jpg";
                    return;
                }

                var path = string.Empty;
                if (imageObject.GetType() == typeof (Movie))
                    path = "Movies";
                var filepath = Path.Combine("Cache", path, imageObject.ImagePath.TrimStart('/'));
                if (!File.Exists(filepath))
                {
                    await DownloadImageAsync(imageObject.ImagePath, filepath, token);
                }
                imageObject.ImagePath = filepath;
            }
            catch (Exception ex)
            {
                logger.Warn(ex.Message);
                imageObject.ImagePath = @"/TheMovieDb.Client;component/Assets/anonymous.jpg";
            }
        }

        private async Task DownloadImageAsync(string filename, string localpath, CancellationToken token)
        {
            logger.Trace("Downloading Image Async");
            if (!File.Exists(localpath))
            {
                var folder = Path.GetDirectoryName(localpath);
                if (string.IsNullOrEmpty(folder))
                    return;
                if (!Directory.Exists(folder))
                    Directory.CreateDirectory(folder);

                var storage = new StorageClient();
                try
                {
                    using (var fileStream = new FileStream(localpath, FileMode.Create, FileAccess.Write,
                        FileShare.None, short.MaxValue, true))
                    {
                        try
                        {
                            await storage.DownloadAsync(filename, fileStream, token);
                        }
                        catch (Exception ex)
                        {
                            logger.Warn(ex.Message);
                            throw;
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                }
            }
        }
    }
}
﻿using System;
using System.Diagnostics;
using System.Net.TMDb;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using NLog;
using TheMovieDb.Domain.Helpers.Interfaces;
using TheMovieDb.Domain.Repositories.Interfaces;
using Account = TheMovieDb.Domain.Models.Account;

namespace TheMovieDb.Domain.Repositories
{
    public class ServiceRepository : IRepository
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly CancellationToken _cancellationToken = new CancellationToken();

        public ServiceRepository(ISettings settings)
        {
            Settings = settings;
        }

        public ISettings Settings { get; }

        public async Task Search(string query, int page = 1)
        {
            using (var client = new ServiceClient(Settings.ApiKey))
            {
                try
                {
                    var tmdbMovis = await client.SearchAsync(query, Settings.Lang, true, page, _cancellationToken);
                }
                catch (Exception e)
                {
                    logger.Warn(e.ToString());
                    throw;
                }
            }
        }

        public async Task<Account> Login(string login, string password)
        {
            logger.Debug($"Logging with username:{login}");
            using (var client = new ServiceClient(Settings.ApiKey))
            {
                try
                {
                    var requiredToken = await client.LoginAsync(login, password, _cancellationToken);
                    var session = await client.GetSessionAsync(requiredToken, _cancellationToken);
                    var tmdbAccount = await client.Settings.GetAccountAsync(session, _cancellationToken);
                    return Mapper.Map<Account>(tmdbAccount);
                }
                catch (Exception e)
                {
                    logger.Error(e.ToString());
                    return null;
                }
            }
        }
    }
}
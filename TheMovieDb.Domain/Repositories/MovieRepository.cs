﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.TMDb;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using NLog;
using TheMovieDb.Domain.Helpers.Interfaces;
using TheMovieDb.Domain.Models;
using TheMovieDb.Domain.Repositories.Interfaces;
using Collections = TheMovieDb.Domain.Models.Collections;
using Collection = TheMovieDb.Domain.Models.Collection;
using MediaCast = TheMovieDb.Domain.Models.MediaCast;
using MediaCredit = TheMovieDb.Domain.Models.MediaCredit;
using MediaCrew = TheMovieDb.Domain.Models.MediaCrew;
using Movie = TheMovieDb.Domain.Models.Movie;
using Movies = TheMovieDb.Domain.Models.Movies;

namespace TheMovieDb.Domain.Repositories
{
    public class MovieRepository : IRepository
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly ImageRepository _imageRepository;

        public MovieRepository(ImageRepository imageRepository, ISettings settings)
        {
            Settings = settings;
            _imageRepository = imageRepository;
        }

        public ISettings Settings { get; }

        public async Task<Movie> GetMovieAsync(int id, CancellationToken token)
        {
            logger.Debug($"Getting movie {id}");
            try
            {
                using (var client = new ServiceClient(Settings.ApiKey))
                {
                    var tmdbMovie = await client.Movies.GetAsync(id, Settings.Lang, true, token);
                    var movie = Mapper.Map<Movie>(tmdbMovie);
                    return movie;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return null;
            }
        }

        public async Task<List<Movie>> GetTopRatedAsync(int page, CancellationToken token)
        {
            logger.Debug($"Getting top rated movie, page {page}");
            try
            {
                using (var client = new ServiceClient(Settings.ApiKey))
                {
                    var tmdbMovies = await client.Movies.GetTopRatedAsync(Settings.Lang, page, token);
                    var movies = Mapper.Map<Movies>(tmdbMovies);

                    await _imageRepository.DownloadImagesAsync(movies.Results, MediaType.Movies, token);
                    return movies.Results.ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return null;
            }
        }

        public async Task<List<Movie>> GetNowPlayingAsync(int page, CancellationToken token)
        {
            logger.Debug($"Getting now playing movie, page {page}");
            try
            {
                using (var client = new ServiceClient(Settings.ApiKey))
                {
                    var tmdbMovies = await client.Movies.GetNowPlayingAsync(Settings.Lang, page, token);
                    var movies = Mapper.Map<Movies>(tmdbMovies);

                    await _imageRepository.DownloadImagesAsync(movies.Results, MediaType.Movies, token);
                    return movies.Results.ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return null;
            }
        }

        public async Task<List<Movie>> GetPopularAsync(int page, CancellationToken token)
        {
            logger.Debug($"Getting popular movie, page {page}");
            try
            {
                using (var client = new ServiceClient(Settings.ApiKey))
                {
                    var tmdbMovies = await client.Movies.GetPopularAsync(Settings.Lang, page, token);
                    var movies = Mapper.Map<Movies>(tmdbMovies);

                    await _imageRepository.DownloadImagesAsync(movies.Results, MediaType.Movies, token);
                    return movies.Results.ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return null;
            }
        }

        public async Task<List<Movie>> GetUpcomingAsync(int page, CancellationToken token)
        {
            logger.Debug($"Getting upcoming movie, page {page}");
            try
            {
                using (var client = new ServiceClient(Settings.ApiKey))
                {
                    var tmdbMovies = await client.Movies.GetUpcomingAsync(Settings.Lang, page, token);
                    var movies = Mapper.Map<Movies>(tmdbMovies);

                    await _imageRepository.DownloadImagesAsync(movies.Results, MediaType.Movies, token);
                    return movies.Results.ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return null;
            }
        }

        public async Task<IEnumerable<MediaCredit>> GetCreditsAcync(int id, CreditType creditType,
            CancellationToken token, bool downloadImage = true)
        {
            logger.Trace($"Getting movie {id} {creditType}");
            try
            {
                using (var client = new ServiceClient(Settings.ApiKey))
                {
                    var tmdbCredits = await client.Movies.GetCreditsAsync(id, token);
                    IEnumerable<MediaCredit> mediaCredit;
                    switch (creditType)
                    {
                        case CreditType.Cast:
                            mediaCredit =
                                Mapper.Map<IEnumerable<MediaCast>>(tmdbCredits.Where(p => p is System.Net.TMDb.MediaCast));
                            break;
                        case CreditType.Crew:
                            mediaCredit =
                                Mapper.Map<IEnumerable<MediaCrew>>(tmdbCredits.Where(p => p is System.Net.TMDb.MediaCrew));
                            break;
                        default:
                            return null;
                    }

                    if (downloadImage)
                        // The movie credits is person
                        await _imageRepository.DownloadImagesAsync(mediaCredit, MediaType.Persons, token);
                    return mediaCredit;
                }
            }
            catch (Exception ex)
            {
                logger.Warn(ex.Message);
                return null;
            }
        }

        public async Task<Movies> SearchAsync(string query, CancellationToken token, int page = 1, int? year = null)
        {
            logger.Debug($"Search movie {query}, page {page}");
            using (var client = new ServiceClient(Settings.ApiKey))
            {
                try
                {
                    var tmdbMovis =
                        await
                            client.Movies.SearchAsync(query, Settings.Lang, true, year, false, page,
                                CancellationToken.None);
                    var movies = Mapper.Map<Movies>(tmdbMovis);
                    await _imageRepository.DownloadImagesAsync(movies.Results, MediaType.Movies, token);
                    return movies;
                }
                catch (Exception e)
                {
                    logger.Error(e.ToString());
                    return null;
                }
            }
        }

        public async Task<Collections> SearchCollectionAsync(string query, CancellationToken token, int page = 1)
        {
            logger.Debug($"Search movie collection {query}, page {page}");
            try
            {
                using (var client = new ServiceClient(Settings.ApiKey))
                {
                    var tmdbCollections = await client.Collections.SearchAsync(query, Settings.Lang, page, token);
                    var collections = Mapper.Map<Collections>(tmdbCollections);
                    await _imageRepository.DownloadImagesAsync(collections.Results, MediaType.Movies, token);
                    return collections;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return null;
            }
        }

        public async Task<Collection> GetCollectionAsync(int id, CancellationToken token)
        {
            logger.Debug($"Get movie collection, id {id}");
            try
            {
                using (var client = new ServiceClient(Settings.ApiKey))
                {
                    var tmdbCollection = await client.Collections.GetAsync(id, Settings.Lang, true, token);
                    var collection = Mapper.Map<Collection>(tmdbCollection);
                    await _imageRepository.DownloadImagesAsync(collection.Parts, MediaType.Movies, token);
                    return collection;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return null;
            }
        }

        public async Task<Movies> GetGenresAsync(int id, CancellationToken token, int page = 1)
        {
            logger.Debug($"Get Genres movie, id {id}");
            try
            {
                using (var client = new ServiceClient(Settings.ApiKey))
                {
                    var tmdbMovies = await client.Genres.GetMoviesAsync(id, Settings.Lang, true, page, token);
                    var movies = Mapper.Map<Movies>(tmdbMovies);
                    await _imageRepository.DownloadImagesAsync(movies.Results, MediaType.Movies, token);
                    return movies;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return null;
            }
        }
    }
}
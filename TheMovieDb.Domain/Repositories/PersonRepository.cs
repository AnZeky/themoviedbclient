﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.TMDb;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using NLog;
using TheMovieDb.Domain.Helpers.Interfaces;
using TheMovieDb.Domain.Models;
using TheMovieDb.Domain.Repositories.Interfaces;
using Person = TheMovieDb.Domain.Models.Person;
using PersonCast = TheMovieDb.Domain.Models.PersonCast;
using PersonCredit = TheMovieDb.Domain.Models.PersonCredit;
using PersonCrew = TheMovieDb.Domain.Models.PersonCrew;

namespace TheMovieDb.Domain.Repositories
{
    public class PersonRepository : IRepository
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly ImageRepository _imageRepository;

        public PersonRepository(ImageRepository imageRepository, ISettings settings)
        {
            Settings = settings;
            _imageRepository = imageRepository;
        }

        public ISettings Settings { get; }

        public async Task<Person> GetPersonAsync(int id, CancellationToken token)
        {
            logger.Debug($"Getting person {id}");
            try
            {
                using (var client = new ServiceClient(Settings.ApiKey))
                {
                    var tmdbPerson = await client.People.GetAsync(id, true, token);
                    var person = Mapper.Map<Person>(tmdbPerson);
                    return person;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return null;
            }
        }

        public async Task<IEnumerable<PersonCredit>> GetCreditsAcync(int id, CreditType creditType,
            CancellationToken token, bool downloadImage = true)
        {
            logger.Trace($"Getting person {id} {creditType}");
            try
            {
                using (var client = new ServiceClient(Settings.ApiKey))
                {
                    var tmdbCredits =
                        await client.People.GetCreditsAsync(id, Settings.Lang, DataInfoType.Movie, token);
                    IEnumerable<PersonCredit> personCredits;
                    switch (creditType)
                    {
                        case CreditType.Cast:
                            personCredits =
                                Mapper.Map<IEnumerable<PersonCast>>(
                                    tmdbCredits.Where(p => p is System.Net.TMDb.PersonCast));
                            break;
                        case CreditType.Crew:
                            personCredits =
                                Mapper.Map<IEnumerable<PersonCrew>>(
                                    tmdbCredits.Where(p => p is System.Net.TMDb.PersonCrew));
                            break;
                        default:
                            return null;
                    }

                    if (downloadImage)
                        // The person credits is movies
                        await _imageRepository.DownloadImagesAsync(personCredits, MediaType.Movies, token);
                    return personCredits;
                }
            }
            catch (Exception ex)
            {
                logger.Warn(ex.Message);
                return null;
            }
        }

        public async Task<Persons> SearchAsync(string query, CancellationToken token, int page = 1, int? year = null)
        {
            logger.Debug($"Search person {query}, page {page}");
            using (var client = new ServiceClient(Settings.ApiKey))
            {
                try
                {
                    var tmdbMovis =
                        await client.People.SearchAsync(query, true, false, page, token);
                    var movies = Mapper.Map<Persons>(tmdbMovis);
                    await
                        _imageRepository.DownloadImagesAsync(movies.Results, MediaType.Persons, token);
                    return movies;
                }
                catch (Exception e)
                {
                    logger.Error(e.ToString());
                    return null;
                }
            }
        }
    }
}
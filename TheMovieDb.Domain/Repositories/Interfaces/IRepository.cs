﻿using TheMovieDb.Domain.Helpers.Interfaces;

namespace TheMovieDb.Domain.Repositories.Interfaces
{
    public interface IRepository
    {
        ISettings Settings { get; }
    }
}
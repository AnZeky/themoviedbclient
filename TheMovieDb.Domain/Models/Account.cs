﻿namespace TheMovieDb.Domain.Models
{
    public class Account
    {
        public int Id { get; internal set; }
        
        public bool IncludeAdult { get; internal set; }
        
        public string CountryCode { get; internal set; }
        
        public string LanguageCode { get; internal set; }
        
        public string Name { get; internal set; }
        
        public string UserName { get; internal set; }
    }
}
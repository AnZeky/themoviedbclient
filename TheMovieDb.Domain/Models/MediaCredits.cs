﻿using System.Collections.Generic;

namespace TheMovieDb.Domain.Models
{
    public class MediaCredits
    {
        public IList<MediaCast> Cast { get; set; }

        public IList<MediaCrew> Crew { get; internal set; }
    }
}
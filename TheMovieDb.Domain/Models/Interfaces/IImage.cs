﻿namespace TheMovieDb.Domain.Models.Interfaces
{
    public interface IImage
    {
        string ImagePath { get; set; }
    }
}
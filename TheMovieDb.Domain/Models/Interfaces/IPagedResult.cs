﻿namespace TheMovieDb.Domain.Models.Interfaces
{
    public interface IPagedResult
    {
        int PageIndex { get; }

        int PageCount { get; }

        int TotalCount { get; }
    }
}
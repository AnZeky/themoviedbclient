﻿namespace TheMovieDb.Domain.Models.Interfaces
{
    public interface ICredit
    {
        string Role { get; }
    }
}
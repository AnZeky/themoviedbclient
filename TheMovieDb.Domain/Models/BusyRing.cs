﻿using System.Threading;
using TheMovieDb.Domain.Models.Enums;

namespace TheMovieDb.Domain.Models
{
    public class BusyRing
    {
        public BusyRing(bool isWait = true)
        {
            IsWait = isWait;
        }

        public bool IsWait { get; }
    }
}
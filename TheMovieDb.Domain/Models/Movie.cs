﻿using System;
using System.Collections.Generic;
using TheMovieDb.Domain.Models.Interfaces;

namespace TheMovieDb.Domain.Models
{
    public class Movie : IImage
    {
        public int Id { get; internal set; }

        public string Title { get; internal set; }

        public string OriginalTitle { get; internal set; }

        public string TagLine { get; internal set; }

        public string Overview { get; internal set; }

        public string Backdrop { get; internal set; }

        public bool Adult { get; internal set; }

        public Collection BelongsTo { get; internal set; }

        public int Budget { get; internal set; }

        public IEnumerable<Genre> Genres { get; internal set; }

        public string HomePage { get; internal set; }

        public string Imdb { get; internal set; }

        public IEnumerable<Company> Companies { get; internal set; }

        public IEnumerable<Country> Countries { get; internal set; }

        public DateTime? ReleaseDate { get; internal set; }

        public long Revenue { get; internal set; }

        public int? Runtime { get; internal set; }

        //public IEnumerable<Language> Languages { get; internal set; }

        //public AlternativeTitles AlternativeTitles { get; internal set; }

        public MediaCredits Credits { get; internal set; }

        public Images Images { get; internal set; }

        //public Videos Videos { get; internal set; }

        //public Keywords Keywords { get; internal set; }

        //public Releases Releases { get; internal set; }

        //public Translations Translations { get; internal set; }

        public decimal Popularity { get; internal set; }

        public decimal VoteAverage { get; internal set; }

        public int VoteCount { get; internal set; }

        public string Status { get; internal set; }

        public string Poster { get; set; }

        //public ExternalIds External { get; internal set; }

        public string ImagePath
        {
            get { return Poster; }
            set { Poster = value; }
        }
    }
}
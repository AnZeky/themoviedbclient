﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMovieDb.Domain.Models.Interfaces;

namespace TheMovieDb.Domain.Models
{
    public class Collection : IImage
    {
        public int Id { get; internal set; }

        public string Name { get; internal set; }

        public string Poster { get; set; }

        public string Backdrop { get; set; }

        public IEnumerable<Movie> Parts { get; internal set; }

        public string ImagePath
        {
            get { return Poster; }
            set { Poster = value; }
        }
    }
}
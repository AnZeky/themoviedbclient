﻿using System.Collections.Generic;
using TheMovieDb.Domain.Models.Interfaces;

namespace TheMovieDb.Domain.Models
{
    public abstract class PagedResult<T> : IPagedResult
    {
        public IEnumerable<T> Results { get; internal set; }

        public int PageIndex { get; internal set; }

        public int PageCount { get; internal set; }

        public int TotalCount { get; internal set; }
    }

}
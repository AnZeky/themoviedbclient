﻿using System;
using TheMovieDb.Domain.Models.Interfaces;

namespace TheMovieDb.Domain.Models
{
    public abstract class PersonCredit : IImage
    {
        public int Id { get; internal set; }

        public string CreditId { get; internal set; }

        public string Title { get; internal set; }

        public string OriginalTitle { get; internal set; }

        public string Poster { get; internal set; }

        public DateTime? ReleaseDate { get; internal set; }

        public bool Adult { get; internal set; }

        public string ImagePath
        {
            get { return Poster; }
            set { Poster = value; }
        }
    }
}
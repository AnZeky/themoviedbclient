﻿namespace TheMovieDb.Domain.Models
{
    public abstract class MediaCredit
    {
        public int Id { get; internal set; }

        public string CreditId { get; internal set; }

        public string Name { get; internal set; }

        public string Profile { get; internal set; }
    }
}
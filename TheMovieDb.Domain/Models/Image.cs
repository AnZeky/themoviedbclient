﻿using TheMovieDb.Domain.Models.Interfaces;

namespace TheMovieDb.Domain.Models
{
    public class Image : IImage
    {
        public string FilePath { get; internal set; }

        public short Width { get; internal set; }

        public short Height { get; internal set; }

        public string LanguageCode { get; internal set; }

        public decimal AspectRatio { get; internal set; }

        public decimal VoteAverage { get; internal set; }

        public int VoteCount { get; internal set; }

        public string ImagePath
        {
            get { return FilePath; }
            set { FilePath = value; }
        }
    }
}
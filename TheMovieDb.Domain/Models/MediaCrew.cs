﻿using TheMovieDb.Domain.Models.Interfaces;

namespace TheMovieDb.Domain.Models
{
    public class MediaCrew : MediaCredit, ICredit
    {
        public string Department { get; internal set; }

        public string Job { get; internal set; }
        public string Role => Job;
    }
}
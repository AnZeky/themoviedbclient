﻿namespace TheMovieDb.Domain.Models
{
    public class Genre
    {
        public int Id { get; internal set; }

        public string Name { get; internal set; }
    }
}
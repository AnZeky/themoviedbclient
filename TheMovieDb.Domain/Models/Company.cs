﻿namespace TheMovieDb.Domain.Models
{
    public class Company
    {
        public int Id { get; internal set; }

        public string Name { get; internal set; }

        public string Description { get; internal set; }

        public string HeadQuarters { get; internal set; }

        public string HomePage { get; internal set; }

        public Company Parent { get; internal set; }

        public string Logo { get; internal set; }
    }
}
﻿using System.Collections.Generic;

namespace TheMovieDb.Domain.Models
{
    public class PersonImages
    {
        public IEnumerable<Image> Results { get; internal set; }
    }
}
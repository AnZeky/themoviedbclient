﻿using System.Collections.Generic;

namespace TheMovieDb.Domain.Models
{
    public class PersonCredits
    {
        public IEnumerable<PersonCast> Cast { get; internal set; }
        public IEnumerable<PersonCrew> Crew { get; internal set; }
    }
}
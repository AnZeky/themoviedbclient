﻿namespace TheMovieDb.Domain.Models
{
    public enum MediaType
    {
        Movies,
        Persons,
        Collections
    }
}
﻿namespace TheMovieDb.Domain.Models.Enums
{
    public enum BusyIdentifierType
    {
        Shell,
        Poster,
        PersonsCredit,
        ListMovie,
    }
}
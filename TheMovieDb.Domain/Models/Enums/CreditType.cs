﻿namespace TheMovieDb.Domain.Models
{
    public enum CreditType
    {
        Cast,
        Crew
    }
}
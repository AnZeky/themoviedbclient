﻿using System.Collections.Generic;
using TheMovieDb.Domain.Models.Interfaces;

namespace TheMovieDb.Domain.Models
{
    public class Person : IImage
    {
        public int Id { get; internal set; }

        public string Name { get; internal set; }

        public bool Adult { get; internal set; }

        public IEnumerable<string> KnownAs { get; internal set; }

        public string Biography { get; internal set; }

        public string BirthDay { get; internal set; }

        public string DeathDay { get; internal set; }

        public string HomePage { get; internal set; }

        public string BirthPlace { get; internal set; }

        public PersonCredits Credits { get; internal set; }

        public PersonImages Images { get; internal set; }

        public string Poster { get; set; }

        //        public ExternalIds External { get; internal set; }

        public string ImagePath
        {
            get { return Poster; }
            set { Poster = value; }
        }
    }
}
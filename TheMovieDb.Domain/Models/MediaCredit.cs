﻿using TheMovieDb.Domain.Models.Interfaces;

namespace TheMovieDb.Domain.Models
{
    public abstract class MediaCredit : IImage
    {
        public int Id { get; internal set; }

        public string CreditId { get; internal set; }

        public string Name { get; internal set; }

        public string Profile { get; internal set; }

        public string ImagePath
        {
            get { return Profile; }
            set { Profile = value; }
        }
    }
}
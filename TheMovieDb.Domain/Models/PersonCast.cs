﻿using TheMovieDb.Domain.Models.Interfaces;

namespace TheMovieDb.Domain.Models
{
    public class PersonCast : PersonCredit, ICredit
    {
        public string Character { get; internal set; }
        public string Role => Character;
    }
}
﻿using System.Collections.Generic;

namespace TheMovieDb.Domain.Models
{
    public class Images
    {
        public IEnumerable<Image> Backdrops { get; internal set; }

        public IEnumerable<Image> Posters { get; internal set; }
    }
}
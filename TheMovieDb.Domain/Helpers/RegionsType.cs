﻿namespace TheMovieDb.Domain.Helpers
{
    public static class RegionsType
    {
        public static string MainRegion { get; } = "MainRegion";
        public static string MovieCastRegion { get; } = "MovieCastRegion";
        public static string MovieCrewRegion { get; } = "MovieCrewRegion";
        public static string PosterPersonRegion { get; } = "PosterPersonRegion";
        public static string SearchMovieTabRegion { get; } = "SearchMovieTabRegion";
        public static string SearchPersonsTabRegion { get; } = "SearchPersonsTabRegion";
        public static string SearchCollectionsTabRegion { get; } = "SearchCollectionsTabRegion";
        public static string CollectionMovieRegion { get; } = "CollectionMovieRegion";
        public static string GenresMovieRegion { get; } = "GenresMovieRegion";

        public static string PosterMovieRegion { get; } = "PosterMovieRegion";
        public static string PersonCastMovieRegion { get; } = "PersonCastMovieRegion";
        public static string PersonCrewMovieRegion { get; } = "PersonCrewMovieRegion";
        public static string TopRatedMoviesRegion { get; } = "TopRatedMoviesRegion";
        public static string NowPlayingMoviesRegion { get; } = "NowPlayingMoviesRegion";
        public static string PopularMoviesRegion { get; } = "PopularMoviesRegion";
        public static string UpcomingMoviesRegion { get; } = "UpcomingMoviesRegion";
    }
}
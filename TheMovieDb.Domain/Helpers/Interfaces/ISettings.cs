﻿namespace TheMovieDb.Domain.Helpers.Interfaces
{
    public interface ISettings
    {
        string Lang { get; set; }
        string ApiKey { get; set; }
    }
}
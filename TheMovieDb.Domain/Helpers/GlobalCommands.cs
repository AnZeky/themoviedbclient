﻿using System;
using System.Windows.Input;
using Prism.Commands;
using Prism.Regions;
using TheMovieDb.Domain.Models;

namespace TheMovieDb.Domain.Helpers
{
    public static class GlobalCommands
    {
        private static IRegionManager _regionManager;

        static GlobalCommands()
        {
            SelectMovieCommand = new DelegateCommand<int?>(SelectMovie);
            SelectGenresCommand = new DelegateCommand<Genre>(SelectGenres);
            SelectPersonCommand = new DelegateCommand<int?>(SelectPerson);
            SelectCollectionCommand = new DelegateCommand<int?>(SelectCollection);
        }

        public static ICommand SelectMovieCommand { get; }
        public static ICommand SelectGenresCommand { get; }
        public static ICommand SelectPersonCommand { get; }
        public static ICommand SelectCollectionCommand { get; }

        public static void Initialization(IRegionManager region)
        {
            _regionManager = region;
        }

        private static void SelectMovie(int? id)
        {
            if (id == null)
                return;

            var query = new NavigationParameters {{"ID", id}};
            _regionManager.RequestNavigate(RegionsType.MainRegion,
                new Uri("MovieView" + query, UriKind.Relative));
        }

        private static void SelectGenres(Genre genre)
        {
            if (genre == null)
                return;

            var query = new NavigationParameters {{"GENRE", genre } };
            _regionManager.RequestNavigate(RegionsType.MainRegion,
                new Uri("GenresView", UriKind.Relative), query);
        }

        private static void SelectPerson(int? id)
        {
            if (id == null)
                return;

            var query = new NavigationParameters {{"ID", id}};
            _regionManager.RequestNavigate(RegionsType.MainRegion,
                new Uri("PersonView" + query, UriKind.Relative));
        }
        
        private static void SelectCollection(int? id)
        {
            if (id == null)
                return;

            var query = new NavigationParameters {{"ID", id}};
            _regionManager.RequestNavigate(RegionsType.MainRegion,
                new Uri("CollectionView" + query, UriKind.Relative));
        }
    }
}
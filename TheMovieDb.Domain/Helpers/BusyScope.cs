﻿using System;
using System.Threading;
using NLog;
using Prism.Events;
using TheMovieDb.Domain.Events;
using TheMovieDb.Domain.Models;

namespace TheMovieDb.Domain.Helpers
{
    public class BusyScope : IDisposable
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private static IEventAggregator _eventAggregator;
        public BusyScope()
        {
            logger.Debug($"Increase Busy counter:{Count + 1}");
            if (CancellationTokenSource == null)
            {
                logger.Debug($"Creating CancellationTokenSource");
                Count = 0;
                CancellationTokenSource = new CancellationTokenSource();
                New = true;

                _eventAggregator.GetEvent<BusyRingEvent>().Publish(new BusyRing());
            }
            Count++;
        }

        public static void Initialization(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }
        public static int Count { get; private set; }
        public static bool New { get; set; }

        private static CancellationTokenSource CancellationTokenSource { get; set; }

        public CancellationToken Token
            => CancellationTokenSource?.Token ?? CancellationToken.None;

        public void Dispose()
        {
            logger.Debug($"Decrease Busy counter:{Count}");
            if (Count <= 0)
            {
                logger.Warn($"Underflow Busy counter:{Count}");
                return;
            }
            Count--;
            if (Count == 0)
            {
                CancellationTokenSource = null;
                _eventAggregator.GetEvent<BusyRingEvent>().Publish(new BusyRing(false));
            }
        }

        public static void Cancel()
        {
            Count = 0;
            logger.Debug($"Cancel task");
            CancellationTokenSource?.Cancel();
            CancellationTokenSource = null;
        }
    }
}
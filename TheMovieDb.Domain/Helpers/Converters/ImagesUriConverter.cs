﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;

namespace TheMovieDb.Domain.Helpers.Converters
{
    public class ImagesUriConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || value.ToString().StartsWith("/"))
                return value;
            return Path.GetFullPath(value.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace TheMovieDb.Domain.Helpers.Converters
{
    public class LengthTextTrimmerConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return string.Empty;
            if (parameter == null)
                return value;
            int maxLength;
            if (!int.TryParse(parameter.ToString(), out maxLength))
                return value;
            if (value.ToString().Length > maxLength)
                return value.ToString().Substring(0, maxLength) + "...";
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
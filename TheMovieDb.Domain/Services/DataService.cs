using System.Net.TMDb;
using AutoMapper;
using NLog;
using TheMovieDb.Domain.Models;
using TheMovieDb.Domain.Repositories;
using TheMovieDb.Domain.Services.Interfaces;
using Account = System.Net.TMDb.Account;
using Collection = System.Net.TMDb.Collection;
using Collections = System.Net.TMDb.Collections;
using Company = System.Net.TMDb.Company;
using Country = System.Net.TMDb.Country;
using Genre = System.Net.TMDb.Genre;
using Image = System.Net.TMDb.Image;
using Images = System.Net.TMDb.Images;
using MediaCast = System.Net.TMDb.MediaCast;
using MediaCredit = System.Net.TMDb.MediaCredit;
using MediaCredits = System.Net.TMDb.MediaCredits;
using MediaCrew = System.Net.TMDb.MediaCrew;
using Movie = System.Net.TMDb.Movie;
using Movies = System.Net.TMDb.Movies;
using Person = System.Net.TMDb.Person;
using PersonCast = System.Net.TMDb.PersonCast;
using PersonCredit = System.Net.TMDb.PersonCredit;
using PersonCredits = System.Net.TMDb.PersonCredits;
using PersonCrew = System.Net.TMDb.PersonCrew;
using PersonImages = System.Net.TMDb.PersonImages;

namespace TheMovieDb.Domain.Services
{
    public class DataService : IDataService
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        static DataService()
        {
            logger.Debug("DataService - Initializing");
            try
            {
                Mapper.Initialize(cfg =>
                {
                    cfg.CreateMap(typeof (System.Net.TMDb.PagedResult<>), typeof (Models.PagedResult<>));

                    cfg.CreateMap<Movie, Models.Movie>()
                        .ForMember(m => m.ImagePath, opt => opt.Ignore());
                    cfg.CreateMap<Movies, Models.Movies>();
                    cfg.CreateMap<MediaCast, Models.MediaCast>()
                        .ForMember(m => m.ImagePath, opt => opt.Ignore())
                        .ForMember(m => m.Role, opt => opt.Ignore());
                    cfg.CreateMap<MediaCrew, Models.MediaCrew>()
                        .ForMember(m => m.ImagePath, opt => opt.Ignore())
                        .ForMember(m => m.Role, opt => opt.Ignore());
                    cfg.CreateMap<MediaCredit, Models.MediaCredit>()
                        .ForMember(m => m.ImagePath, opt => opt.Ignore());
                    cfg.CreateMap<MediaCredits, Models.MediaCredits>();

                    cfg.CreateMap<Person, Models.Person>()
                        .ForMember(m => m.ImagePath, opt => opt.Ignore());
                    cfg.CreateMap<People, Persons>();
                    cfg.CreateMap<PersonCast, Models.PersonCast>()
                        .ForMember(m => m.ImagePath, opt => opt.Ignore());
                    cfg.CreateMap<PersonCredit, Models.PersonCredit>()
                        .ForMember(m => m.ImagePath, opt => opt.Ignore());
                    cfg.CreateMap<PersonCredits, Models.PersonCredits>();
                    cfg.CreateMap<PersonCrew, Models.PersonCrew>()
                        .ForMember(m => m.ImagePath, opt => opt.Ignore());
                    cfg.CreateMap<PersonImages, Models.PersonImages>();

                    cfg.CreateMap<Collection, Models.Collection>()
                        .ForMember(m => m.ImagePath, opt => opt.Ignore());
                    cfg.CreateMap<Collections, Models.Collections>();
                    cfg.CreateMap<Account, Models.Account>();
                    cfg.CreateMap<Image, Models.Image>()
                        .ForMember(m => m.ImagePath, opt => opt.Ignore());
                    cfg.CreateMap<Images, Models.Images>();
                    cfg.CreateMap<Genre, Models.Genre>();
                    cfg.CreateMap<Company, Models.Company>();
                    cfg.CreateMap<Country, Models.Country>();
                });

                Mapper.AssertConfigurationIsValid();
            }
            catch (AutoMapperConfigurationException ex)
            {
                logger.Error(ex.Message);
            }
        }

        public DataService(MovieRepository movie, PersonRepository person, ImageRepository image,
            ServiceRepository service)
        {
            Image = image;
            Movie = movie;
            Person = person;
            Service = service;
        }

        public ImageRepository Image { get; set; }
        public MovieRepository Movie { get; set; }
        public PersonRepository Person { get; set; }
        public ServiceRepository Service { get; set; }
    }
}
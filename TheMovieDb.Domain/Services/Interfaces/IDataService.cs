﻿using TheMovieDb.Domain.Repositories;

namespace TheMovieDb.Domain.Services.Interfaces
{
    public interface IDataService
    {
        ImageRepository Image { get; set; }
        MovieRepository Movie { get; set; }
        PersonRepository Person { get; set; }
        ServiceRepository Service { get; set; }
    }
}
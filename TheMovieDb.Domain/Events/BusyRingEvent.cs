﻿using Prism.Events;
using TheMovieDb.Domain.Models;

namespace TheMovieDb.Domain.Events
{
    public class BusyRingEvent : PubSubEvent<BusyRing>
    {
    }
}
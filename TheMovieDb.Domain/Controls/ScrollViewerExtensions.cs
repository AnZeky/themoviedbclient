﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace TheMovieDb.Domain.Controls
{
    public class ScrollViewerExtensions : DependencyObject
    {
        public static readonly DependencyProperty IsHorizontalScrollOnWheelProperty =
            DependencyProperty.RegisterAttached("IsHorizontalScrollOnWheel", typeof (bool),
                typeof (ScrollViewerExtensions), new PropertyMetadata(false, OnIsHorizontalScrollOnWheel));

        public static readonly DependencyProperty IsScrollOnWheelControlProperty =
            DependencyProperty.RegisterAttached("IsScrollOnWheelControl", typeof (bool),
                typeof (ScrollViewerExtensions), new PropertyMetadata(false, OnIsScrollOnWheelControl));

        public static bool GetIsHorizontalScrollOnWheel(DependencyObject sender)
        {
            return (bool) sender.GetValue(IsHorizontalScrollOnWheelProperty);
        }

        public static void SetIsHorizontalScrollOnWheel(DependencyObject sender, bool value)
        {
            sender.SetValue(IsHorizontalScrollOnWheelProperty, value);
        }

        private static void OnIsHorizontalScrollOnWheel(DependencyObject sender,
            DependencyPropertyChangedEventArgs e)
        {
            var scrollViewer = sender as ScrollViewer;
            if (scrollViewer == null) return;
            if ((bool) e.NewValue)
                scrollViewer.PreviewMouseWheel += HorizontalMouseWheel;
            else
                scrollViewer.PreviewMouseWheel -= HorizontalMouseWheel;
        }

        private static void HorizontalMouseWheel(object sender, MouseWheelEventArgs e)
        {
            var scrollviewer = sender as ScrollViewer;
            if (scrollviewer != null)
                if (e.Delta > 0)
                {
                    scrollviewer.LineLeft();
                    scrollviewer.LineLeft();
                }
                else
                {
                    scrollviewer.LineRight();
                    scrollviewer.LineRight();
                }
            e.Handled = true;
        }

        public static bool GetIsScrollOnWheelControl(DependencyObject sender)
        {
            return (bool) sender.GetValue(IsScrollOnWheelControlProperty);
        }

        public static void SetIsScrollOnWheelControl(DependencyObject sender, bool value)
        {
            sender.SetValue(IsScrollOnWheelControlProperty, value);
        }

        private static void OnIsScrollOnWheelControl(DependencyObject sender,
            DependencyPropertyChangedEventArgs e)
        {
            var scrollViewer = sender as ScrollViewer;
            if (scrollViewer == null)
                return;
            if ((bool) e.NewValue)
                scrollViewer.PreviewMouseWheel += MouseWheelControl;
            else
                scrollViewer.PreviewMouseWheel -= MouseWheelControl;
        }

        private static void MouseWheelControl(object sender, MouseWheelEventArgs e)
        {
            var handle = (Keyboard.Modifiers & ModifierKeys.Control) > 0;
            if (handle || e.RightButton == MouseButtonState.Pressed)
            {
                var scrollViewer = sender as ScrollViewer;
                if (scrollViewer == null)
                    return;
                if (e.Delta > 0)
                {
                    scrollViewer.LineUp();
                    scrollViewer.LineUp();
                }
                else
                {
                    scrollViewer.LineDown();
                    scrollViewer.LineDown();
                }
                e.Handled = true;
            }
        }
    }
}